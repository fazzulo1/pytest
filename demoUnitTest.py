import unittest

class TestStringMethods(unittest.TestCase):

    def test_upper(self):
        self.assertEqual('foo'.upper(), 'FOO')

    def test_isupper(self):
        self.assertTrue('foo'.isupper())
        self.assertFalse('Foo'.isupper())

    def test_split(self):
        s = 'hello world'
        self.assertEqual(s.split(), ['hello', 'world'])
        # check that s.split fails when the separator is not a string
        with self.assertRaises(TypeError):
            s.split(2)

#!/usr/bin/env python

import unittest

class SortedDict(dict):
    """
    Dictionary that maintains keys in sorted order (by time of definition).
    """
    def __init__(self, *args, **kwargs):
        super(SortedDict, self).__init__(*args, **kwargs)
        self._key_order = []

    def __setitem__(self, key, value):
        if key not in self:
            self._key_order.append(key)
        super(SortedDict, self).__setitem__(key, value)

    def keys(self):
        return self._key_order

    # rest of implementation

class TestSortedDict(unittest.TestCase):
    def setUp(self):
        self.d = SortedDict()

    def test_inserted_item_can_be_accessed(self):
        self.d['some-key'] = 1
        self.assertEqual(1, self.d['some-key'])

    def test_keys_maintains_order(self):
        self.d['some-key'] = 1
        self.d['another-key'] = 42
        self.d[('tuples', 'can', 'be', 'keys', 'too')] = None
        self.assertEqual(
            ['some-key', 'another-ke', ('tuples', 'can', 'be', 'keys', 'too')],
            self.d.keys()
        )


if __name__ == '__main__':
    unittest.main()