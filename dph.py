from numbers import Number
import re

def add_DPH(prise: Number) -> Number:
    """
    Spočítá cenu s dph 21%.
    """
    return prise * 1.21