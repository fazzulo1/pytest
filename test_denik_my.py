import pytest

from denik import phi, tableFor

@pytest.fixture
def jurnal():
    return [
        {"events":["carrot","exercise","weekend"],"squirrel":False},
        {"events":["bread","pudding","brushed teeth","weekend","touched tree"],"squirrel":False},
        {"events":["carrot","nachos","brushed teeth","cycling","weekend"],"squirrel":False}
    ]

def test_table_for_carrot(jurnal):
    # act
    result = tableFor("carrot", jurnal)
    # asserts
    assert result == [1, 2, 0, 0]

def test_table_for_weekend(jurnal):
    # act
    result = tableFor("weekend", jurnal)
    # asserts
    assert result == [0, 3, 0, 0]

def test_table_for_cycling(jurnal):
    # act
    result = tableFor("cycling", jurnal)
    # asserts
    assert result == [2, 1, 0, 0]

@pytest.mark.parametrize("event, table", [
    ("carrot",  [1, 2, 0, 0]),
    ("weekend", [0, 3, 0, 0]),
    ("cycling", [2, 1, 0, 0])
])
def test_table_for(jurnal, event, table):
    # act
    result = tableFor(event, jurnal)
    # asserts
    assert result == table

def test_phi():
    # act
    result = phi(76, 9, 4, 1)
    # asserts
    assert round(result, 9) == 0.068599434 

def test_phi_with_zeros():
    # act
    result = phi(1, 0, 0, 0)
    # asserts
    assert round(result, 9) == 0.068599434 


