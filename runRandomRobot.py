
from State import State
from itertools import count
 
if __name__=="__main__":
    city = loadCity(roads = [
        "Alice's House-Bob's House",   "Alice's House-Cabin",
        "Alice's House-Post Office",   "Bob's House-Town Hall",
        "Daria's House-Ernie's House", "Daria's House-Town Hall",
        "Ernie's House-Grete's House", "Grete's House-Farm",
        "Grete's House-Shop",          "Marketplace-Farm",
        "Marketplace-Post Office",     "Marketplace-Shop",
        "Marketplace-Town Hall",       "Shop-Town Hall"]
    )
    places = list(city.keys())
    baliky = list(islice(iRandomBaliky(places), 10))
    start_state = State('Post Office', baliky)
    
    from randomRobot import randomRobot
    run(start_state, randomRobot, city)