import pytest

from robot import city

@pytest.fixture(scope='session')
def roads_A_B_C():
    return ['A-B', 'A-C']

@pytest.fixture(scope='session')
def roads_A_B_C_D(roads_A_B_C):
    return roads_A_B_C + ["D-D"]

def test_load_city_ABCD(roads_A_B_C_D):
    # Arrange
    #roads = ['A-B', 'A-C']
    # Act
    result = city.loadCity(roads_A_B_C_D)
    # Assert
    assert len(result) == 4
    assert 'B' in result['A']
    assert 'A' in result['C']


def test_load_city_ABC(roads_A_B_C):
    # Arrange
    #roads = ['A-B', 'A-C']
    # Act
    result = city.loadCity(roads_A_B_C)
    # Assert
    assert len(result) == 3
    assert 'B' in result['A']
    assert 'A' in result['C']