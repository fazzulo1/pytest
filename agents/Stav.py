from operator import itemgetter
from collections import namedtuple
from pickle import load

with open("agents/cities.pkl", mode="br") as pkl:
     cities = load(pkl)
     
     
Zakazka = namedtuple('Zakazka', ['location', 'destination'])

class Stav:

    def __init__(self, position, contracts: list[Zakazka] = []):
        self.__position = position
        self.__contracts = contracts

    def __repr__(self):
        return f"Stav[position='{self.__position}', contracts={self.__contracts}]"
        
    @property
    def contracts(self):
        return list(self.__contracts)

    @property
    def position(self):
        return self.__position
        
    def move(self, destination, cities=cities):
        """_summary_

        >>> stav = Stav('Brno', [])
        >>> stav.position
        'Brno'
        
        Args:
            destination (_type_): _description_
            cities (_type_, optional): _description_. Defaults to cities.

        Returns:
            _type_: _description_
        """
        # Kdyz se nemuzu pohnout, vratim stejny stav
        if destination not in map(itemgetter(0), cities[self.__position]):
            return self

        contracts = []
        for contract in self.__contracts:
            assert isinstance(contract, Zakazka), f"List of contract do not contat only contract"
            if contract.location != self.__position:
                contracts.append(contract)
            else:
                z = Zakazka(destination, contract.destination)
                if z.location != z.destination:
                    contracts.append(z)

        return Stav(destination, contracts)
    
if __name__ == "__main__":
    import doctest
    doctest.testmod()
