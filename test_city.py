from robot.city import loadCity

import pytest

# ['A-B','B-C','C-A']
# ['C-B','B-A','A-C']
# ['C-B','B-A','A-C','A-B','B-C','C-A']

@pytest.mark.xfail
@pytest.mark.parametrize('roads', (
    ['A-B','B-C','C-A'],
    ['C-B','B-A','A-C'],
    ['C-B','B-A','A-C','A-B','B-C','C-A']))
def test_loadCity(city_ABC, roads):
    # ACT
    city = loadCity(roads)
    # ASSERT
    assert city == city_ABC